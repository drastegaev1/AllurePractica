package pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class YandexMainPage {
    By searhField = new By.ByCssSelector("#text");

    public  YandexMainPage(WebDriver driver){
        this.driver = driver;}

    WebDriver driver;

    @Step("открытие главной страницы")
    public YandexMainPage open(){
        driver.get("http://ya.ru");
        return this;
    }
    @Step("Вход в поисковую строку {text}")
    public void enterText(String text){
        driver.findElement(searhField).sendKeys(text + Keys.ENTER);
    }
}
