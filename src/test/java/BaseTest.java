import io.github.bonigarcia.wdm.WebDriverManager;

import io.qameta.allure.Attachment;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BaseTest {
    WebDriver driver;

    @Before
    public void setUp(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    @After
    public void close() {
        if (driver != null)
        driver.quit();
    }
    @Attachment(value = "screen", type = "image/png")
    public byte[] getScreenshot(){
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);

    }
}

